package com.yy.bigevent.pojo;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class Area {
    @NotNull
    private Integer areaID;
    @NotNull
    private Integer floorID;
    @NotEmpty
    private String areaName;
}
