package com.yy.bigevent.service.impl;

import com.yy.bigevent.mapper.FloorMapper;
import com.yy.bigevent.pojo.Floor;
import com.yy.bigevent.service.FloorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class FloorServiceImpl implements FloorService {
    @Autowired
    private FloorMapper floorMapper;

    //根据用户名查找用户
    @Override
    public Floor findByFloorName(String floorname) {
        Floor f = floorMapper.findByFloorName(floorname);
        return f;
    }

    //注册
    @Override
    public void add(String floorname) {
        //添加
        floorMapper.add(floorname);
    }
}
