package com.yy.bigevent.controller;

import com.yy.bigevent.pojo.Result;
import com.yy.bigevent.pojo.Floor;
import com.yy.bigevent.service.FloorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/floor")
public class FloorController {
    @Autowired
    private FloorService floorService;

    //添加新楼层
    @PostMapping("/add")
    public Result add(String floorname){
        //查询楼层名
        Floor f = floorService.findByFloorName(floorname);
        if(f==null){
            //没有占用
            //添加新楼层
            floorService.add(floorname);
            return Result.success();
        }
        else{
            //占用
            return Result.error("已有该楼层");
        }

    }
}
