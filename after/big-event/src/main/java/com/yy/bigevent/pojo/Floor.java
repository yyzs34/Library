package com.yy.bigevent.pojo;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class Floor {
    @NotNull
    private Integer floorID;//主键ID
    @NotEmpty
    private String floorName;//楼层名称
}