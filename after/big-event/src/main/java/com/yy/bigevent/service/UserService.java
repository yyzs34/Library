package com.yy.bigevent.service;

import com.yy.bigevent.pojo.User;

public interface UserService {

    //更新用户基本信息
    void update(User user);

    //根据用户名查询用户
    User findByUserName(String username);
    //注册
    void register(String username, String password);

    void updatePassword(String newpwd);
}
