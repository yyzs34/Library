package com.yy.bigevent.service.impl;

import com.yy.bigevent.mapper.UserMapper;
import com.yy.bigevent.pojo.User;
import com.yy.bigevent.service.UserService;
import com.yy.bigevent.utils.Md5Util;
import com.yy.bigevent.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    //根据用户名查找用户
    @Override
    public User findByUserName(String username) {
        User u = userMapper.findByUserName(username);
        return u;
    }
    //注册
    @Override
    public void register(String username, String password) {
        //加密
        String md5String = Md5Util.getMD5String(password);
        //添加
        userMapper.add(username,md5String);
    }
    //更新用户密码

    @Override
    public void updatePassword(String newpwd) {
        //加密
        String md5String = Md5Util.getMD5String(newpwd);
        Map<String,Object> map = ThreadLocalUtil.get();
        Integer id = (Integer) map.get("id");
        userMapper.updatePassword(md5String,id);
    }

    //更新用户基本信息
    @Override
    public void update(User user){
        user.setUpdateTime(LocalDateTime.now());
        userMapper.update(user);
    }


}
