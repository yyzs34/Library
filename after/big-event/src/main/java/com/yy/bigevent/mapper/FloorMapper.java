package com.yy.bigevent.mapper;

import com.yy.bigevent.pojo.Floor;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface FloorMapper {

    //根据楼层名查询楼层
    @Select("select * from floors where FloorName=#{floorname}")
    Floor findByFloorName(String floorname);

    //添加
    @Insert("insert into floors(floorname)"+
            " values(#{floorname})")
    void add(String floorname);
}
