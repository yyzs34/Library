package com.yy.bigevent.service;

import com.yy.bigevent.pojo.Floor;

public interface FloorService {

    //根据楼层名查询楼层
    Floor findByFloorName(String floorName);

    //添加楼层
    void add(String floorname);

}
