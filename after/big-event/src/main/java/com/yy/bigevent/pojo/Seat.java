package com.yy.bigevent.pojo;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class Seat {
    @NotNull
    private Integer seatID;
    @NotNull
    private Integer areaID;
    @NotEmpty
    private String seatNumber;
    @NotEmpty
    private String seatStatus; // 应该限制为 '空闲', '占用', '不可用'
}