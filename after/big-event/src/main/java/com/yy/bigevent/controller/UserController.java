package com.yy.bigevent.controller;

import com.yy.bigevent.pojo.Result;
import com.yy.bigevent.pojo.User;
import com.yy.bigevent.service.UserService;
import com.yy.bigevent.utils.JwtUtil;
import com.yy.bigevent.utils.Md5Util;
import com.yy.bigevent.utils.ThreadLocalUtil;
import jakarta.validation.constraints.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
@RestController
@RequestMapping("/user")
@Validated
public class UserController {

    @Autowired
    private UserService userService;

    //注册
    @PostMapping("/register")
    public Result register(@Pattern(regexp = "^\\S{5,16}$") String username, @Pattern(regexp = "^\\S{5,16}$") String password){
        //查询用户
        User u = userService.findByUserName(username);
        if(u==null){
            //没有占用
            //注册
            userService.register(username,password);
            return Result.success();
        }
        else{
            //占用
            return Result.error("用户名已被占用");
        }

    }
    //登录
    @PostMapping("/login")
    public Result<String> login(@Pattern(regexp = "^\\S{5,16}$") String username, @Pattern(regexp = "^\\S{5,16}$") String password){
        //根据用户名查询用户
        User loginUser = userService.findByUserName(username);
        //判断用户是否存在
        if(loginUser==null){
            return Result.error("用户名错误");
        }
        //判断密码是否正确
        if(Md5Util.getMD5String(password).equals(loginUser.getPassword())){
            //登录成功
            Map<String,Object> claims = new HashMap<>();
            claims.put("id", loginUser.getId());
            claims.put("username", loginUser.getUsername());
            String token = JwtUtil.genToken(claims);
            return Result.success(token);
        }
        return Result.error("密码错误");
    }
    //获取用户信息
    @GetMapping("/userInfo")
    public Result<User> userInfo(/*@RequestHeader(name = "Authorization") String token*/){
    /*  Map<String,Object> map = JwtUtil.parseToken(token);
        String username = (String)map.get("username");*/

        Map<String,Object> map = ThreadLocalUtil.get();
        String username = (String)map.get("username");
        //根据用户名查询用户
        User user = userService.findByUserName(username);
        return Result.success(user);
    }
    //更新用户基本信息
    @PutMapping("/update")
    public Result update(@RequestBody @Validated User user){

        userService.update(user);
        return Result.success();

    }
    //修改用户密码
    @PutMapping("/updatePassword")
    public Result updatePassword(@RequestBody Map<String,String> params){
        String oldpwd = params.get("oldpwd");
        String newpwd = params.get("newpwd");
        String repwd = params.get("repwd");
        //判断数据是否为空
        if(!StringUtils.hasLength(oldpwd) ||!StringUtils.hasLength(newpwd)||!StringUtils.hasLength(repwd)){
            return Result.error("数据不能为空");
        }
        //比对oldpwd是否和数据库的pwd相同
        Map<String,Object> map= ThreadLocalUtil.get();
        String username = (String)map.get("username");
        User loginUser = userService.findByUserName(username);
        if(!loginUser.getPassword().equals(Md5Util.getMD5String(oldpwd))){
            return Result.error("原密码错误");
        }

        if(!newpwd.equals(repwd)){
            return Result.error("两次密码不一致");
        }

        userService.updatePassword(newpwd);
        return Result.success();
    }


}
