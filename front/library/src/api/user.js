//导入request.js
import request from '../utils/request'

//注册
export const registerService = (registerData) => {
    //json转换成
    var params = new URLSearchParams()
    for (let key in registerData) {
        params.append(key, registerData[key])
    }
    return request.post('/user/register', params)
}

//登录
export const userLoginService = (loginData) =>{

    var params = new URLSearchParams()
    for (let key in loginData) {
        params.append(key, loginData[key])
    }
    return request.post('/user/login', params)
}

//获取个人信息
export const userInfoGetService = ()=>{
    return request.get('/user/userInfo');
}

//修改个人信息
export const userInfoUpdateService = (userInfo)=>{
    return request.put('/user/update',userInfo)
}

//修改密码
export const userPasswordUpdateService = (userPassword)=>{
    return request.put('/user/updatePassword',userPassword)
}

