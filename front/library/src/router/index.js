import {createRouter,createWebHistory} from 'vue-router'

import login from '@/views/login.vue'
import layout from '@/views/layout.vue'
import UserInfo from '@/views/user/UserInfo.vue'
import UserAvatar from '@/views/user/UserAvatar.vue'
import UserResetPassword from '@/views/user/UserResetPassword.vue'
import notice from '@/views/notice.vue'
import reservaion from '@/views/reservation.vue'
//路由关系
const routes = [

    {path:'/login',component:login},
    {path:'/',component:layout,redirect:'/notice',
    children:[
        { path: '/user/info', component: UserInfo },
        { path: '/user/avatar', component: UserAvatar },
        { path: '/user/password', component: UserResetPassword },
        {path:'/notice',component:notice},
        {path:'/reservation',component:reservaion},
    ]},
    

]

 //创建路由器
 const router = createRouter({

    history:createWebHistory(),
    routes:routes
 })

//导出路由
export default router
